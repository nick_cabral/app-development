//
//  CameraViewController.m
//  07 Camera
//
//  Created by Michael Ziray on 2/27/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "CameraViewController.h"
#import "SuboverViewController.h"

@interface CameraViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cameraButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *customButton;
@property(strong, nonatomic)UIPopoverController *cameraPopoverController;
@property(strong, nonatomic)UIPopoverController *customPopoverController;
@end

@implementation CameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)launchCustomPopover:(id)sender
{
    SuboverViewController *subController = [[SuboverViewController alloc] initWithNibName:@"SuboverViewController" bundle: nil];
    
    self.customPopoverController = [[UIPopoverController alloc] initWithContentViewController: subController];
    [self.customPopoverController setPopoverContentSize:subController.view.frame.size animated: YES];
    
    
    [self.customPopoverController presentPopoverFromBarButtonItem: self.customButton
                              permittedArrowDirections: UIPopoverArrowDirectionAny
                                              animated: YES];
}

- (IBAction)launchCamera:(id)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];

    if([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera] )
    {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }

    // This gives a warning since we're not implementing the UINavigationControllerDelegate protocol
    [imagePickerController setDelegate: self];


    self.cameraPopoverController = [[UIPopoverController alloc] initWithContentViewController: imagePickerController];
    
    [self.cameraPopoverController presentPopoverFromBarButtonItem: self.cameraButton
                              permittedArrowDirections: UIPopoverArrowDirectionAny
                                              animated: YES];
    // Alternatively, we can use a modal to take up the whole screen
//    [self presentViewController: imagePickerController animated: YES completion: nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.cameraPopoverController dismissPopoverAnimated: YES];
    [picker dismissViewControllerAnimated: YES completion: nil];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [self.imageView setImage: image];
}









@end
