//
//  WebServicesController.h
//  04 Bronco Maps
//
//  Created by Michael Ziray on 1/30/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ASIHTTPRequest.h"


@interface WebServicesController : NSObject

+(void)downloadBuildingsDataWithDelegate:(id<ASIHTTPRequestDelegate>)delegate;


@end





