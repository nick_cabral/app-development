//
//  AppDelegate.h
//  04 Bronco Maps
//
//  Created by Michael Ziray on 1/30/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
