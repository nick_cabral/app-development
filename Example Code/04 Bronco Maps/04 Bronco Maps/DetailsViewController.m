//
//  DetailsViewController.m
//  04 Bronco Maps
//
//  Created by Michael Ziray on 2/6/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "DetailsViewController.h"
#import "LocationController.h"


@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *Subtitle;

@end

@implementation DetailsViewController



- (IBAction)savePin:(id)sender
{
    NSString *title = [self.titleTextField text];
    NSString *description = [self.Subtitle text];
    
//    CLLocationCoordinate2D coordinate = [[LocationController currentLocation].coordinate];
    
    [self dismissViewControllerAnimated: YES completion: nil];
}




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
