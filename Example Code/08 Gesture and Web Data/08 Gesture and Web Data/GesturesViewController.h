//
//  GesturesViewController.h
//  08 Gesture and Web Data
//
//  Created by Michael Ziray on 3/4/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GesturesViewController : UIViewController <UIPopoverControllerDelegate, UIImagePickerControllerDelegate, NSURLConnectionDelegate>

@end
