//
//  GesturesViewController.m
//  08 Gesture and Web Data
//
//  Created by Michael Ziray on 3/4/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "GesturesViewController.h"
#import "ManipulativeView.h"


@interface GesturesViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *customButton;
@property(strong, nonatomic)UIPopoverController *cameraPopoverController;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property(strong, nonatomic)NSURLConnection *urlConnection;
@property(strong, nonatomic)NSMutableData *responseData;
@property (weak, nonatomic) IBOutlet UIView *draggableView;

@end

@implementation GesturesViewController

- (IBAction)swipeGestureHandler:(id)sender
{
    UIView *view = (UIView *)sender;
    NSLog(@"Swiped");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.responseData = [[NSMutableData alloc] init];
    
//    NSURL *url = [NSURL URLWithString:@"http://www.broncosports.com/sports/m-footbl/sched/bosu-m-footbl-sched.html"];
//    NSURLRequest *urlRequest = [NSURLRequest requestWithURL: url];
//    NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest: urlRequest delegate: self];
	// Do any additional setup after loading the view.
    
    CGRect frame = self.draggableView.frame;
    CGRect manipulativeViewFrame = CGRectMake(frame.size.width/2-200, frame.size.height/2-200, 200, 200);
    
    // Add helmet
    ManipulativeView *helmetManipulativeView = [[ManipulativeView alloc] initWithFrame: manipulativeViewFrame];
    helmetManipulativeView.imageView.image = [UIImage imageNamed: @"BoiseState-Nike_Helmet"];
    helmetManipulativeView.containerView = self.draggableView;
    [self.draggableView addSubview: helmetManipulativeView];
    
    // Add jersey
    ManipulativeView *jerseyManipulativeView = [[ManipulativeView alloc] initWithFrame: manipulativeViewFrame];
    jerseyManipulativeView.imageView.image = [UIImage imageNamed: @"BoiseState-Jersey"];
    jerseyManipulativeView.containerView = self.draggableView;
    [self.draggableView addSubview: jerseyManipulativeView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)launchCamera:(id)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera] )
    {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    // This gives a warning since we're not implementing the UINavigationControllerDelegate protocol
    [imagePickerController setDelegate: self];
    
    
//    self.cameraPopoverController = [[UIPopoverController alloc] initWithContentViewController: imagePickerController];
    
//    [self.cameraPopoverController presentPopoverFromBarButtonItem: self.customButton
//                                         permittedArrowDirections: UIPopoverArrowDirectionAny
//                                                         animated: YES];
    // Alternatively, we can use a modal to take up the whole screen
    [self presentViewController: imagePickerController animated: YES completion: nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.cameraPopoverController dismissPopoverAnimated: YES];
    [picker dismissViewControllerAnimated: YES completion: nil];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [self.imageView setImage: image];
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    
    NSLog(@"%i", [urlResponse statusCode]);
    if( ([urlResponse statusCode]/100) == 2 )
    {
        [self.responseData setLength: 0];
    }
}

/*
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse
{
//    NSLog(@"request %@", request);
}
*/
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //    NSLog(@"data %@", data);
    [_responseData appendData: data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{

    NSString *responseString = [[NSString alloc] initWithData: self.responseData  encoding: NSUTF8StringEncoding ];
    
    NSScanner *scanner = [NSScanner scannerWithString: responseString];
    NSString *newString = @"";
    [scanner scanUpToString: @"<table 'id'=\"schedtable\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">" intoString: &newString];

    while ( [scanner isAtEnd] )
    {
        [scanner scanUpToString:@"<td class=\"row-text\">" intoString: &newString];
        
        NSString *dateString;
        [scanner scanUpToString:@"</td>" intoString: &dateString];
        
        [scanner scanUpToString:@"<td class=\"row-text\">" intoString: &newString];
        
        NSString *opponentString;
        [scanner scanUpToString:@"</td>" intoString: &opponentString];
        
        NSLog(@"");
        
    }
    
    
}

@end
