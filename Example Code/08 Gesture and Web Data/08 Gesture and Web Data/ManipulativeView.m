//
//  ManipulativeView.m
//  BSU Football
//
//  Created by Michael Ziray on 7/31/12.
//  Copyright (c) 2012 Z Studio Labs. All rights reserved.
//

#import "ManipulativeView.h"
#import <QuartzCore/QuartzCore.h>


@interface ManipulativeView ()
@property(nonatomic, strong)UIView *containerView;
@property(nonatomic, strong)UIImageView *imageView;
@end



@implementation ManipulativeView

@synthesize imageView, containerView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        //self.layer.borderWidth = 1.0f;
        //self.layer.borderColor = [[UIColor blackColor] CGColor];
        
        
        // Setup image view
        CGRect imageFrame = self.frame;
        imageFrame.origin.x = 10;
        imageFrame.origin.y = 10;
        imageFrame.size.height -= 20;
        imageFrame.size.width  -= 20;
//        self.layer.borderColor = [[UIColor yellowColor] CGColor];
//        self.layer.borderWidth = 2.0f;
        
        self.imageView = [[UIImageView alloc] initWithFrame: imageFrame];
        [self.imageView setContentMode: UIViewContentModeScaleAspectFit];
        
        //[imageView setImage: [UIImage imageNamed:@"BoiseState-Nike_Helmet.png"]];
        [self addSubview: imageView];
        
        // Initialization code
        UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self  action:@selector(rotatePiece:)];
        [rotationGesture setDelegate: self];
        [self addGestureRecognizer:rotationGesture];
        
        UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
        [pinchGesture setDelegate:self];
        [self addGestureRecognizer:pinchGesture];
        
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(panPiece:)];
        [panGesture setDelegate: self];
        [self addGestureRecognizer: panGesture];
        
        UILongPressGestureRecognizer *tapAndHoldGesture = [[UILongPressGestureRecognizer alloc] initWithTarget: self action: @selector(popupContextMenu:)];
        [tapAndHoldGesture setDelegate: self];
        [tapAndHoldGesture setMinimumPressDuration: 1.2f];
        [self addGestureRecognizer: tapAndHoldGesture];
        
        
        [self setAutoresizingMask: UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
        
    }
    return self;
}

-(void)awakeFromNib
{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (void)panPiece:(UIPanGestureRecognizer *)gestureRecognizer
{
    [self.containerView bringSubviewToFront: self];
    
    [self adjustAnchorPointForGestureRecognizer: gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        
        CGPoint translation = [gestureRecognizer translationInView:[gestureRecognizer.view superview]];
        [gestureRecognizer.view setCenter:CGPointMake([gestureRecognizer.view center].x + translation.x, [gestureRecognizer.view center].y+translation.y)];
        [gestureRecognizer setTranslation:CGPointZero inView:[gestureRecognizer.view superview]];
    }
}

- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer
{
    [self.containerView bringSubviewToFront: self];
    
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        [gestureRecognizer setScale:1];
    }
}


- (void)rotatePiece:(UIRotationGestureRecognizer *)gestureRecognizer
{
    [self.containerView bringSubviewToFront: self];
    
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        [gestureRecognizer view].transform = CGAffineTransformRotate([[gestureRecognizer view] transform], [gestureRecognizer rotation]);
        [gestureRecognizer setRotation:0];
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}


-(void)popupContextMenu:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
//        NSLog(@"inside if");
        CGPoint location = [gestureRecognizer locationInView:[gestureRecognizer view]];
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        UIMenuItem *resetMenuItem = [[UIMenuItem alloc] initWithTitle:@"Remove" action:@selector(menuItemClicked:)];
        
        NSAssert([self becomeFirstResponder], @"Sorry, UIMenuController will not work with %@ since it cannot become first responder", self);
        [menuController setMenuItems:[NSArray arrayWithObject:resetMenuItem]];
        [menuController setTargetRect:CGRectMake(location.x, location.y, 0.0f, 0.0f) inView:[gestureRecognizer view]];
        [menuController setMenuVisible:YES animated:YES];
    
    }
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if(action == @selector(menuItemClicked:))
        return YES;
    else
        return NO;
}

-(void)menuItemClicked:(id)sender
{
//    NSLog(@"%@", [sender class]);
    [self removeFromSuperview];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    /*
     // if the gesture recognizers are on different views, don't allow simultaneous recognition
     if (gestureRecognizer.view != otherGestureRecognizer.view)
     return NO;
     
     // if either of the gesture recognizers is the long press, don't allow simultaneous recognition
     if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
     return NO;
     */
    return YES;
}



- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}


@end
