//
//  BitcoinPrice.h
//  Core Data
//
//  Created by Michael Ziray on 2/13/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BitcoinPrice : NSManagedObject

@property (nonatomic, retain) NSDecimalNumber * lastPrice;
@property (nonatomic, retain) NSDate * timestamp;

@end
