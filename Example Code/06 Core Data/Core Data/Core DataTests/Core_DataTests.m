//
//  Core_DataTests.m
//  Core DataTests
//
//  Created by Michael Ziray on 2/13/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Core_DataTests : XCTestCase

@end

@implementation Core_DataTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
