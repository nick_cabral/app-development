//
//  MainViewController.m
//  Core Data
//
//  Created by Michael Ziray on 2/13/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "MainViewController.h"

#import "AppDelegate.h"
#import "BitcoinPrice.h"

#import "DetailsViewController.h"


@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic)NSArray *bitcoinPrices;
@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSManagedObjectContext *moc = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    @try{
    NSEntityDescription *entityDescription =  [NSEntityDescription entityForName: @"Employee"//NSStringFromClass([BitcoinPrice class])
                                                          inManagedObjectContext: moc];
    
    [fetchRequest setEntity: entityDescription];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"lastPrice" ascending: YES];
    [fetchRequest setSortDescriptors: @[sortDescriptor]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lastPrice > 580"];
    [fetchRequest setPredicate: predicate];
    
    NSError *error;
    self.bitcoinPrices = [moc executeFetchRequest: fetchRequest error: &error ];
    
    [self.tableView reloadData];
	// Do any additional setup after loading the view.
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDelegate Methods
/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return 10.0f;
 }
 
 - (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 
 }
 
 - (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 
 }
 
 - (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
 {
 
 }
 
 - (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
 {
 
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
}



#pragma mark - UITableView Data Source Delegate Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.bitcoinPrices count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/*
 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
 {
 
 }
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableViewCellIdentifier";
    
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if( tableViewCell == nil )
    {
        tableViewCell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
    }
    
    BitcoinPrice *bitcoinPrice = [self.bitcoinPrices objectAtIndex: indexPath.row];
    tableViewCell.textLabel.text = [bitcoinPrice.lastPrice stringValue];
    
    return tableViewCell;
}


@end
