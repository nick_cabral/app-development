//
//  ViewController.m
//  Signin App
//
//  Created by Michael Ziray on 1/23/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property(nonatomic, strong)NSArray *usernamesArray;

@end


@implementation ViewController

-(IBAction)downloadUserInfo:(id)sender
{
    
}

- (IBAction)resetFields
{
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
}



- (IBAction)signin:(id)sender
{
    NSString *username = self.usernameTextField.text;
    NSString *password = [[self usernameTextField] text];
    
    for (NSString *currentUsername in self.usernamesArray)
    {
        if([currentUsername isEqualToString: username])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"Success!"
                                                                message: [NSString stringWithFormat:@"Password: %@", password]
                                                               delegate: self
                                                      cancelButtonTitle: @"Cancel"
                                                      otherButtonTitles: @"other titles", nil];
            [alertView show];
        }
    }
    
}






- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.usernamesArray = [NSArray arrayWithObjects: @"mike", @"bsu", nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
