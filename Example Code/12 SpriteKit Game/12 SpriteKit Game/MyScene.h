//
//  MyScene.h
//  12 SpriteKit Game
//

//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MyScene : SKScene <SKPhysicsContactDelegate>

@end
