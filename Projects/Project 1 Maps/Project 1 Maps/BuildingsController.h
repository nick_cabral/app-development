//
//  BuildingsController.h
//  Project 1 Maps
//
//  Created by Michael Ziray on 2/13/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BuildingAnnotation.h"
#import "ASIHTTPRequest.h"



@interface BuildingsController : NSObject<ASIHTTPRequestDelegate>


+(NSArray *)buildings;
+(void)addBuildings:(NSArray *)newBuildings;
+(void)addBuilding:(BuildingAnnotation *)newBuildingAnnotation;

+(void)refreshBuildings;

@end
