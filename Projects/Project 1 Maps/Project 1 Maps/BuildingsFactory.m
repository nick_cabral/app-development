//
//  BuildingsFactory.m
//  Project 1 Maps
//
//  Created by Michael Ziray on 2/18/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "BuildingsFactory.h"
#import <MapKit/MapKit.h>
#import "BuildingAnnotation.h"
#import "BuildingsController.h"


static BuildingsFactory *sharedBuildingsFactory = nil;


@implementation BuildingsFactory

+(void)buildingsFromJSON:(NSData *)buildingsJSON
{
    NSArray *buildingsArray = [NSJSONSerialization JSONObjectWithData:buildingsJSON options: NSJSONReadingAllowFragments
                                                                error: nil];
    
    NSMutableArray *buildingsAnnotations = [[NSMutableArray alloc] init];
    
    
    for (NSDictionary *buildingInfo in buildingsArray)
    {
        NSString *buildingName = (NSString *)[buildingInfo objectForKey: @"name"];
        NSString *buildingDescription = (NSString *)[buildingInfo objectForKey:@"description"];
        
        
        NSDictionary *locationInfo = (NSDictionary *)[buildingInfo objectForKey:@"location"];
        
        NSNumber * latitude = (NSNumber *)[locationInfo objectForKey:@"latitude"];
        NSNumber * longitude = (NSNumber *)[locationInfo objectForKey: @"longitude"];
        
        CLLocationCoordinate2D buildingLocation;
        buildingLocation.latitude  = latitude.doubleValue;
        buildingLocation.longitude = longitude.doubleValue;
        
        // Construct annotation
        BuildingAnnotation *buildingAnnotation = [[BuildingAnnotation alloc] init];
        buildingAnnotation.title = buildingName;
        buildingAnnotation.subtitle = buildingDescription;
        buildingAnnotation.coordinate = buildingLocation;
        
        // Place annotation on map
        [buildingsAnnotations addObject: buildingAnnotation];
	}
    
    [BuildingsController addBuildings: buildingsAnnotations];
    
}

@end
