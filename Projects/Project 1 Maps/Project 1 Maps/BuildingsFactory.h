//
//  BuildingsFactory.h
//  Project 1 Maps
//
//  Created by Michael Ziray on 2/18/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuildingsFactory : NSObject


+(void)buildingsFromJSON:(NSString *)buildingsJSON;



@end
